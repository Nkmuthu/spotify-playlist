import React from 'react';
import './track.css';

class Track extends React.Component {
    constructor(props) {
        super(props);
        this.addToPlayList = this.addToPlayList.bind(this);
        this.removeFromPlayList = this.removeFromPlayList.bind(this);
    }

    addToPlayList() {
        this.props.addToPlayList(this.props.track);
    }

    removeFromPlayList() {
        this.props.removeFromPlayList(this.props.track);
    }

    render() {
        const { track, remove } = this.props;
        return (
            <div className="track-box">
                {
                    remove ? <div className="action" onClick={this.removeFromPlayList}> - </div> :
                        <div className="action" onClick={this.addToPlayList}> + </div>
                }
                <h3 className="track-name">{track.name}</h3>
                <iframe
                    src={`https://open.spotify.com/embed/track/${track.id}`}
                    width="100%"
                    height="80"
                    frameBorder="0"
                    allowtransparency="true"
                    allow="encrypted-media"
                ></iframe>
                <p className="track-dt">{track.album} &nbsp; | &nbsp; {track.artist}</p>
            </div>
        )
    }
}

export default Track;