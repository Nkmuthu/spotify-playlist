import React from 'react';
import './App.css';
import Search from './Search';
import Spotify from './util/Spotify';
import Track from './Track';
import Playlist from './Playlist';
const Spo = new Spotify();


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchResults: [],
      playListName: 'New Playlist',
      playList: [],
    }
    this.search = this.search.bind(this);
    this.addToPlayList = this.addToPlayList.bind(this);
    this.onChangePlaylistName = this.onChangePlaylistName.bind(this);
    this.removeFromPlayList = this.removeFromPlayList.bind(this);
    this.savePlaylist = this.savePlaylist.bind(this);
  }

  search(term) {
    Spo.search(term).then(data => {
      this.setState({ searchResults: data });
    });
  }

  addToPlayList(track) {
    const { playList } = this.state;
    let index = playList.findIndex(tra => tra.id === track.id);
    if (index === -1) {
      playList.push(track);
    }
    this.setState({ playList });
  }

  onChangePlaylistName(event) {
    this.setState({ playListName: event.target.value });
  }

  removeFromPlayList(track) {
    let { playList } = this.state;
    playList = playList.filter(tra => tra.id !== track.id);
    this.setState({ playList });
  }

  savePlaylist() {
    const trackUris = this.state.playList.map(track => track.uri);
    Spo.savePlaylist(this.state.playListName, trackUris).then(() => {
      this.setState({
        playListName: 'New Playlist',
        playList: []
      });
    });
  }

  render() {
    return (
      <div className="App">
        <h1 className="app-head">My Music</h1>
        <Search onSearch={this.search} />
        <div className="app-playlist-area">
          <div className="search-results">
            {
              this.state.searchResults.map(track => {
                return (
                  <Track key={'re' + track.id} track={track} addToPlayList={this.addToPlayList} />
                )
              })
            }
          </div>
          <Playlist
            playList={this.state.playList}
            playListName={this.state.playListName}
            onChangePlaylistName={this.onChangePlaylistName}
            removeFromPlayList={this.removeFromPlayList}
            savePlaylist={this.savePlaylist} />
        </div>
      </div>
    );
  }
}

export default App;
