import React from 'react';
import './search.css';

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchTerm: '',
        }
        this.onChangeSearch = this.onChangeSearch.bind(this);
        this.search = this.search.bind(this);
        this.onEnter = this.onEnter.bind(this);
    }

    onChangeSearch(event) {
        this.setState({ searchTerm: event.target.value });
    }

    search() {
        this.props.onSearch(this.state.searchTerm);
    }

    onEnter(event) {
        if (event.keyCode === 13) {
            this.search();
        }
    }

    render() {
        return (
            <div className="seach-box">
                <input className="search-input" onChange={this.onChangeSearch} onKeyUp={this.onEnter} />
                <button className="search-btn" onClick={this.search}>Search</button>
            </div>
        )
    }
}

export default Search;