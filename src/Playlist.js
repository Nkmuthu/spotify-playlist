import React from 'react';
import Track from './Track';

class Playlist extends React.Component {
    constructor(props) {
        super(props);
        this.onChangePlaylistName = this.onChangePlaylistName.bind(this);
        this.removeFromPlayList = this.removeFromPlayList.bind(this);
        this.savePlaylist = this.savePlaylist.bind(this);
    }

    onChangePlaylistName(event) {
        this.props.onChangePlaylistName(event);
    }

    removeFromPlayList(track) {
        this.props.removeFromPlayList(track);
    }

    savePlaylist() {
        this.props.savePlaylist();
    }

    render() {
        const { playListName, playList } = this.props;
        return (
            <div className="playlist">
                <input className="playlist-name" placeholder="Enter Playlist Name" value={playListName} onChange={this.onChangePlaylistName} />
                {
                    playList.map(track => {
                        return (
                            <Track key={'ply' + track.id} track={track} remove={true} removeFromPlayList={this.removeFromPlayList} />
                        )
                    })
                }
                {
                    playList.length > 0 && <button className="save-btn" onClick={this.savePlaylist}>Save Playlist</button>
                }
            </div>
        )
    }
}

export default Playlist;