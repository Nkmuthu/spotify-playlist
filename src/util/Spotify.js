const clientId = '71e4afe9b4934e43bc20985f0738c054';
const redirectUrl = 'http://localhost:3000/';
const baseUrl = 'https://api.spotify.com/v1';
let accessToken;
let headers;


const status = (response) => {
    if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
    } else {
        return Promise.reject(new Error(response.statusText))
    }
}

const json = (response) => {
    return response.json()
}

export default class Spotify {
    getAccessToken() {
        if (accessToken) {
            return accessToken;
        }
        const accessTokenMatch = window.location.href.match(/access_token=([^&]*)/);
        const expiresInMatch = window.location.href.match(/expires_in=([^&]*)/);
        if (accessTokenMatch && expiresInMatch) {
            accessToken = accessTokenMatch[1];
            const expiresIn = Number(expiresInMatch[1]);
            window.setTimeout(() => (accessToken = ''), expiresIn * 1000);
            window.history.pushState('Access Token', null, '/');
            headers = { Authorization: `Bearer ${accessToken}` };
            return accessToken;
        } else {
            const scopes = 'playlist-modify-public'
            const accessUrl = 'https://accounts.spotify.com/authorize' +
                '?response_type=token' +
                '&client_id=' + clientId +
                (scopes ? '&scope=' + encodeURIComponent(scopes) : '') +
                '&redirect_uri=' + encodeURIComponent(redirectUrl);
            window.location = accessUrl;
        }
    }

    search(term) {
        const token = this.getAccessToken();
        return fetch(`${baseUrl}/search?type=track&q=${term}`, {
            headers: headers
        })
            .then(status)
            .then(json)
            .then(res => {
                if (!res.tracks) {
                    return [];
                }
                return res.tracks.items.map(track => ({
                    id: track.id,
                    name: track.name,
                    artist: track.artists[0].name,
                    album: track.album.name,
                    uri: track.uri
                }));
            })

    }

    savePlaylist(name, trackUris) {
        if (!name || !trackUris.length) {
            return
        }
        const token = this.getAccessToken();
        return fetch(`${baseUrl}/me`, {
            headers: headers
        })
            .then(status)
            .then(json)
            .then(res => {
                const userId = res.id;
                return fetch(`${baseUrl}/users/${userId}/playlists`, {
                    headers: headers,
                    method: 'POST',
                    body: JSON.stringify({ name: name })
                })
                    .then(status)
                    .then(json)
                    .then(resp => {
                        const playlistId = resp.id;
                        return fetch(`${baseUrl}/users/${userId}/playlists/${playlistId}/tracks`, {
                            headers: headers,
                            method: 'POST',
                            body: JSON.stringify({ uris: trackUris })
                        })
                    })

            })
    }
}